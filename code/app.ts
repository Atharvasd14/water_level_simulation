var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");
let C: geometry.circle;
var Slider1 =  <HTMLInputElement> document.getElementById("Slider_1");
var Slider2 =  <HTMLInputElement> document.getElementById("Slider_2");
Slider1.value = "35";
Slider2.value = "50";
var and_x:number;
var and_y:number;
var centre_x,centre_y;
var hasinitialized:boolean = false;
var circleinitialize:boolean = false;
drawrect (200,250,100,150,"#9766A1",Slider1);
drawrect (200,50,100,150,"blue",Slider2)
var x1,y1,x2,y2,w,h;
x1 = 200;
y1 = 250;
w = 100;
h = 150;
x2 =200;
y2=50;


function start()
{
    if (!hasinitialized)
    {
    hasinitialized = true;
    let a1 = new geometry.point(0,0);
    let c1 = new geometry.point(0,0);
    C = new geometry.circle(c1,a1,4,context);
    
    animate();
    }
}
function drawblocks(x1:number,y1:number,w:number,h:number,h1:number) {
    var p:number =  y1 +  h - h1;
    p = p-5;
    context.beginPath();
    context.lineWidth = 3;
    context.strokeStyle="black"
    context.rect(x1+w,p,5,5);
    context.stroke();
}
function drawallgates(color:string = "grey")
{
    
    drawnotgate(x2+w+5,y2+h-128-5,color);
    drawandgate(and_x,and_y);
    context.beginPath()
    context.fillStyle=color;
    context.arc(centre_x+30+20,centre_y,20,0,2*Math.PI);
    context.fill();

}
function drawnotgate(x:number,y:number,color:string)
{
    var color1:string;
    var color2:string;
    if (color == "grey")
    {
        color2="grey";
        color1="green";
    }
    else 
    {
        color1="grey";
        color2="green";
    }
    context.save();
    context.beginPath();
    context.moveTo(x,y);
    context.lineTo(x+30,y);
    context.strokeStyle=color1;
    context.stroke();
    context.beginPath();
    context.strokeStyle="Black";
    context.moveTo(x+30,y);
    context.lineTo(x+30,y-30);
    context.lineTo(x+60,y);
    context.lineTo(x+30,y+30);
    context.lineTo(x+30,y);
    context.stroke();
    
    context.beginPath();

    context.arc(x+60+2,y,3,0,2*Math.PI);
    context.stroke();
    context.restore();

    context.beginPath();
    context.strokeStyle=color2;
    context.moveTo(x+62,y);
    context.lineTo(x+62+30,y);
    context.lineTo(x+62+30,y+155); 
    context.lineTo(x+62+60,y+155);
    and_x=x+62+60;
    and_y=y+140;
    context.stroke();
}
function drawandgate(x:number,y:number,color:string = "grey")
{
    context.beginPath();
    context.strokeStyle="black";
    context.moveTo(x,y);
    context.lineTo(x,y+50);
    context.lineTo(x+50,y+50);
    context.moveTo(x,y);
    context.lineTo(x+50,y);
    context.stroke();
    context.beginPath();
    context.strokeStyle="black";
    
    context.arc(x+50,y+25,25,-Math.PI/2,Math.PI/2);
    
    centre_x=x+75;
    centre_y=y+25;
    context.stroke();
    context.beginPath();
    context.strokeStyle=color;
    context.moveTo(x1+w+5,y1+h-30-7);
    context.lineTo(x1+w+35,y1+h-30-7);
    context.lineTo(x1+w+35,y1+h-150);
    context.lineTo(x1+w+125,y1+h-150);
    context.stroke();
    context.beginPath();
    context.strokeStyle = "grey";
    context.moveTo(centre_x,centre_y);
    
    context.lineTo(centre_x+30,centre_y);
    if (!circleinitialize)
    {
        let a1 = new geometry.point(centre_x+30+20,centre_y);
        let c1 = new geometry.point(centre_x+30+36,centre_y);
        C = new geometry.circle(c1,a1,2,context);
        circleinitialize=true;
    }
    context.stroke();

}
function drawrect(x: number,y:number,w:number,h:number,color:string,Slider:any) {

    
    drawblocks(x1,y1,w,h,35);
    drawblocks(x2,y2,w,h,130);
    context.beginPath();
    context.strokeStyle = "black";
    context.lineWidth= 3;
    context.rect(x,y,w,h);
    context.stroke();
    context.beginPath();
    context.strokeStyle = "black";
    context.lineWidth= 3;
    context.rect(x,y,w,h);
    context.stroke();
    var p = parseFloat(Slider.value);
    context.rect(x,y,w,20);
    context.fillStyle = color;
    context.fillRect(x,h+y,w,-p);
    
}


function animate()
{
   

    context.clearRect(0,0,800,800);
    
    drawrect(200,250,100,150,"#9766A1",Slider1);
    drawallgates();
    drawrect (200,50,100,150,"blue",Slider2); 
    var temp:number = parseFloat(Slider2.value);
    if (temp<130)
    {
        drawnotgate(x2+w+5,y2+h-128-5,"green");
    }
    else
    {
        drawnotgate(x2+w+5,y2+h-128-5,"grey");
    }
    var temp1:number =parseFloat(Slider1.value);
    if (temp1>35)
    {
        drawandgate(and_x,and_y,"green");
    }
    else
    {
        drawandgate(and_x,and_y,"grey");
    }

    if (temp1>35 && temp<130)
    {
       
        
        if (temp<=131)
            {
                context.beginPath();
                context.strokeStyle = "green";
                context.moveTo(centre_x,centre_y);
                
                context.lineTo(centre_x+30,centre_y);
                context.stroke();

                temp = temp + temp1/50;
                C.updateangle(temp1/50);
                 context.strokeStyle="yellow";
                 C.draw();
            }
            Slider2.value ="" + temp;
    }
   
    window.requestAnimationFrame(animate);
}