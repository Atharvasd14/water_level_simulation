var geometry;
(function (geometry) {
    class point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    geometry.point = point;
    class circle {
        constructor(centre, arbtpt, r, context) {
            this.centre = centre;
            this.arbtpt = arbtpt;
            this.r = r;
            this.getangle();
            this.context = context;
            this.color = "black";
            this.getlength();
        }
        getangle() {
            this.angle = Math.atan((this.centre.y - this.arbtpt.y) / (this.centre.x - this.arbtpt.x));
            this.angle = this.angle * 180 / Math.PI;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.centre.x, this.centre.y, this.r, 0, 2 * Math.PI, false);
            this.context.lineWidth = 3;
            this.context.color = "yellow";
            this.context.fillStyle = "yellow";
            this.context.fill();
            this.context.stroke();
        }
        updateangle(x) {
            this.angle = this.angle + x * 3;
            this.centre.x = this.arbtpt.x + this.length * Math.cos(this.angle * Math.PI / 180);
            this.centre.y = this.arbtpt.y + this.length * Math.sin(this.angle * Math.PI / 180);
        }
        getlength() {
            this.length = Math.pow((this.arbtpt.x - this.centre.x), 2) + Math.pow((this.arbtpt.y - this.centre.y), 2);
            this.length = Math.sqrt(this.length);
        }
    }
    geometry.circle = circle;
})(geometry || (geometry = {}));
//# sourceMappingURL=geometry.js.map